<?php
namespace Redenge\MeasureCode\Presenters;

use View;
use Block;

class AdformPresenter extends Presenter
{	
	private $tplpath;
	
	function __construct(\Redenge\MeasureCode\IMeasureCodeModule $measureCode) 
	{
		parent::__construct($measureCode);
		
		$this->tplpath = __DIR__ . '/../templates/adform';			
	}
	
	public function renderTracking()
	{	
		$shop = $this->core->ShopModel;
		$control = $this->core->ShopController;
		
		$view = new View('tracking.tpl', $this->tplpath);
		
		$pagetype = $this->measureCode->getPageType();
		
		if(is_null($pagetype))
		{
			return;			
		}
		
		$view->pagename = $this->parsePageType($pagetype);
		$view->tracking_id = $this->measureCodeValue->getValue('adform', 'tracking_id');
		
		$orderId = $this->core->ShopController->order->getOrderId();
                
        if($orderId != NULL)
		{
			$shop->order->load(array('id' => $orderId));
			
			if($shop->order->id > 0) 
			{
				$view->order = new Block();
				
				$poplatky =  $shop->order->delivery_price_with_tax + $shop->order->payment_price_with_tax;

				$view->order->total_price = $control->roundPrice( ($shop->order->total_price_with_tax - $poplatky) );
				$view->order->currency = $shop->order->currency;
				$view->order->country_iso = $shop->order->country_iso;
				
				$join = 'JOIN product ON product.code = order_item.code';
				$itemr = $shop->order->item->getRecords('product.id AS product_id, order_item.*', $join, null, 'order_item.id_order=' . $shop->order->id);

				$items = array();
				while ($product = mysqli_fetch_assoc($itemr)) 
				{
					$item = new \stdClass();				
					$item->productid = $product['product_id'];
					$item->productname = $product['name'];
					$item->productcount = $product['item_count'];
					$item->productsales = round($product['discount_price_with_tax'] / $product['item_count'], 2);

					$items[] = $item;
				}

				mysqli_free_result($itemr);
				
				$view->order->items = json_encode($items);
			}
		}
		
		
		return $view->render();
	}
	
	private function parsePageType($_pagetype)
	{
		switch($_pagetype)
		{
			case 'home':
				return 'Home Page';
				
			case 'staticpage':
				return 'Static Page';
				
			case 'store':
				return 'Store Page';
				
			case 'category':
				return 'Category Page';
				
			case 'product':
				return 'Product Page';
			
			case 'cart':
				return 'Cart Page';
			
			case 'purchase':
				return 'Conversion Page';
			
			case 'searchresults':
				return 'Search Page';

		}
	}
}
