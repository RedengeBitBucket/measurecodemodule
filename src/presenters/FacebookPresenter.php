<?php
namespace Redenge\MeasureCode\Presenters;

use View;
use Block;

class FacebookPresenter extends Presenter
{	
	private $tplpath;
	
	function __construct(\Redenge\MeasureCode\IMeasureCodeModule $measureCode) 
	{		
		parent::__construct($measureCode);
		
		$this->tplpath = __DIR__ . '/../templates/facebook';
	}


	/**
	 *
	 * @param array|NULL $params
	 * @param bool $productRestrict
	 * @return string
	 */
	public function renderConversion($params = NULL, $productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('facebook', 'conversion_code');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count($restrict) > 0 && count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}
		$view = new View('conversion.tpl', $this->tplpath);	
		$view->code = $code;
		
		return $view->render();
	}


	public function renderRemarketingProduct($productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('facebook', 'remarketing_code_product');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}
		$view = new View('conversion.tpl', $this->tplpath);	
		$view->code = $code;
		
		return $view->render();
	}


	public function renderTracking()
	{	
		$view = new View('tracking.tpl', $this->tplpath);
		
		
		$id_products = $this->measureCode->getIdProducts();
		$pagetype = $this->measureCode->getPageType();
		$pagename = $this->measureCode->getPageName();
		$pagecategory = $this->measureCode->getPageCategory();
		$totalvalue = $this->measureCode->getTotalValue();
		
		if(is_null($pagetype))
		{
			return;			
		}

		
		$view->event = new Block();
		$view->event->pagetype = $this->parsePageType($pagetype);

		
		if(!is_null($totalvalue))
		{
			$view->event->block_value = new Block();
			$view->event->block_value->value = $this->parseValue($totalvalue);
			
			$currency = $this->measureCode->getCurrency();
			if(!is_null($currency))
			{
				$view->event->block_value->currency = $currency;
			}
		}
		
		
		if(!is_null($pagename))
		{
			$view->event->block_pagename = new Block();
			$view->event->block_pagename->pagename = $pagename;
		}
		
		
		if(!is_null($pagecategory))
		{
			$view->event->block_pagecategory = new Block();
			$view->event->block_pagecategory->pagecategory = $this->parsePageCategory($pagecategory);
		}
		
		
		if(!is_null($id_products))
		{
			$view->event->block_product = new Block();
			
			if(count($id_products) > 1)
			{
				$view->event->block_product->type = 'product_group';
			}
			else
			{
				$view->event->block_product->type = 'product';
			}
			
			
			$view->event->block_product->id_products = $this->parseIdProducts($id_products);			
		}
		
		
		$view->tracking_id = $this->measureCodeValue->getValue('facebook', 'pixel_id');
		
		return $view->render();
	}
	
	private function parseIdProducts($_idProducts)
	{
		if(is_null($_idProducts) || sizeof($_idProducts) == 0)
		{
			return null;
		}
		else if(sizeof($_idProducts) == 1)
		{
			return sprintf("['%d']",$_idProducts[0]);
		}
		else if(sizeof($_idProducts) > 1)
		{
			return sprintf('[%s]', implode(',',array_map(function($v){return sprintf("'%d'", $v);}, $_idProducts)));	
		}
	}
	
	private function parsePageCategory($_pagecategory)
	{
		if(is_array($_pagecategory))
		{
			return implode(' > ', $_pagecategory);
		}
		else
		{
			return $_pagecategory;
		}
	}
	
	private function parsePageType($_pagetype)
	{
		switch($_pagetype)
		{
			case 'home':
			case 'staticpage':
			case 'store':
			case 'category':
			case 'product':
				return 'ViewContent';
			
			case 'cart':
				return 'InitiateCheckout';
			
			case 'purchase':
				return 'Purchase';
			
			case 'searchresults':
				return 'Search';

		}
	}
	
	private function parseValue($_value)
	{
		return str_replace(',', '.', sprintf('%.2f', floatval($_value)));
	}
}
