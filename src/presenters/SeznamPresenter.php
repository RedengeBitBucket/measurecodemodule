<?php
namespace Redenge\MeasureCode\Presenters;

use View;
use Block;
use Strings;

class SeznamPresenter extends Presenter
{
	private $tplpath;


	function __construct(\Redenge\MeasureCode\IMeasureCodeModule $measureCode) 
	{
		parent::__construct($measureCode);
		
		$this->tplpath = __DIR__ . '/../templates/seznam';
	}


	/**
	 * Deprecated
	 * @return type
	 */
	public function renderConversion()
	{
		$view = new View('conversion.tpl', $this->tplpath);
		

		$orderId = $this->core->ShopController->order->getOrderId();

		if($orderId != NULL)
		{
			$conversion_order_code = $this->measureCodeValue->getValue('seznam', 'conversion_code');
			
			$this->shop->order->load(array(
				'id' => $orderId
			));
			
			if(Strings::length($conversion_order_code) > 0 && $this->shop->order->id > 0)
			{
				$conversion_order_code = preg_replace('/@TOTAL_VALUE/', $this->shop->order->total_price_with_tax, $conversion_order_code);
				
				$view->conversion_order = new Block();
				$view->conversion_order->code = $conversion_order_code;
			}
		}
		
		return $view->render();
	}


	/**
	 *
	 * @param array|NULL $params
	 * @param bool $productRestrict
	 * @return string
	 */
	public function renderConversionNew($params = NULL, $productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('seznam', 'conversion_code');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count($restrict) > 0 && count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}

		$totalPriceWithTax = isset($params['totalPriceWithTax']) ? $params['totalPriceWithTax'] : '';
		$code = preg_replace('/@TOTAL_VALUE/', $totalPriceWithTax, $code);

		$view = new View('conversion.tpl', $this->tplpath);	
		$view->conversion_order = new Block();
		$view->conversion_order->code = $code;
		
		return $view->render();
	}


	public function renderRemarketingProduct($productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('seznam', 'remarketing_code_product');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}
		$view = new View('remarketing.tpl', $this->tplpath);	
		$view->code = $code;
		
		return $view->render();
	}

}
