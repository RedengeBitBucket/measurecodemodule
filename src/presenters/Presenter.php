<?php
namespace Redenge\MeasureCode\Presenters;

class Presenter 
{
	protected $core;
	protected $shop;
	
	protected $measureCode;
	protected $measureCodeValue;
	
	function __construct(\Redenge\MeasureCode\IMeasureCodeModule $measureCode) 
	{
		$this->core = $measureCode->core;
		$this->shop = $measureCode->core->ShopModel;
		
		$this->measureCode = $measureCode;
		$this->measureCodeValue = $measureCode->measureCodeValue;		
	}
}
