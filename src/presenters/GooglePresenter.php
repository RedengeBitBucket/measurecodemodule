<?php
namespace Redenge\MeasureCode\Presenters;

use View;
use Block;

class GooglePresenter extends Presenter
{	
	private $tplpath;
	
	function __construct(\Redenge\MeasureCode\IMeasureCodeModule $measureCode) 
	{
		parent::__construct($measureCode);
		
		$this->tplpath = __DIR__ . '/../templates/google';
	}
	
	public function renderAnalytics()
	{
		$shop = $this->core->ShopModel;
		
		$view = new View('analytics.tpl', $this->tplpath);
		
		$view->account_code = $this->measureCodeValue->getValue('google', 'analytics_key');
		
		
		$orderId = $this->core->ShopController->order->getOrderId();
                
        if($orderId != NULL)
		{
			$shop->order->load(array('id' => $orderId));
			
			if($shop->order->analysis_sent <= 0) 
			{
				$this->renderEcommerce($view);
			}
		}
		
		return $view->render();
	}
	
	private function renderEcommerce($view) 
	{
		$shop = $this->core->ShopModel;
		$control = $this->core->ShopController;


		$view->ecommerce_block = new Block;
		$view->ecommerce_block->index = $shop->order->index;

		$poplatky =  $shop->order->delivery_price + $shop->order->payment_price;

		$view->ecommerce_block->total_price = $control->roundPrice( ($shop->order->total_price - $poplatky) );
		$view->ecommerce_block->tax = $control->roundPrice( ($shop->order->total_price_with_tax - $shop->order->total_price) );
		$view->ecommerce_block->delivery_price = $control->roundPrice( $poplatky );
		$view->ecommerce_block->invoice_city = $shop->order->city;
		$view->ecommerce_block->invoice_country = $shop->order->country;			

		$itemr = $shop->order->item->getRecords(null, null, null, 'order_item.id_order=' . $shop->order->id);

		while ($product = mysqli_fetch_assoc($itemr)) 
		{
			$view->ecommerce_block->item = new Block;
			$view->ecommerce_block->item->index = $shop->order->index;
			$view->ecommerce_block->item->code = $product['code'];
			$view->ecommerce_block->item->name = $product['name'];
			$view->ecommerce_block->item->price = $control->roundPrice($product['discount_price_with_tax'] / $product['item_count']);
			$view->ecommerce_block->item->item_count = $product['item_count'];
		}

		mysqli_free_result($itemr);		
	}
	
	public function renderDynamicRemarketing()
	{	
		$view = new View('dynamicRemarketing.tpl', $this->tplpath);
		
		
		$id_products = $this->measureCode->getIdProducts();
		
		if(is_null($id_products) || sizeof($id_products) == 0)
		{
			$id_products = null;
		}
		else if(sizeof($id_products) == 1)
		{
			$id_products = $id_products[0];
		}
		else if(sizeof($id_products) > 1)
		{
			$id_products = sprintf('[%s]', implode(',',array_map(function($v){return sprintf('%d', $v);}, $id_products)));	
		}
		
		
		
		$pagetype = $this->measureCode->getPageType();
		
		switch($pagetype)
		{
			case 'home':
				$pagetype = 'home';
				break;
			
			case 'cart':
				$pagetype = 'cart';
				break;
			
			case 'purchase':
				$pagetype = 'purchase';
				break;
			
			case 'category':
				$pagetype = 'category';
				break;
			
			case 'searchresults':
				$pagetype = 'searchresults';
				break;
			
			case 'product':
				$pagetype = 'product';
				break;
			
			case 'staticpage':
			case 'store':	
				$pagetype = 'other';
				break;
		}
		
		
		$totalvalue = $this->measureCode->getTotalValue();
		
		if(!is_null($totalvalue))
		{
			$totalvalue = str_replace(',', '.', sprintf('%.2f', floatval($totalvalue)));
		}
		
		
		
		if(is_null($id_products) && is_null($pagetype) && is_null($totalvalue))
		{
			return;
		}
		
		if(!is_null($id_products))
		{
			$view->block_ids = new Block();
			$view->block_ids->id_products = $id_products;
		}
		
		if(!is_null($pagetype))
		{
			$view->block_pagetype = new Block();
			$view->block_pagetype->pagetype = $pagetype;
		}
		
		if(!is_null($totalvalue))
		{
			$view->block_totalvalue = new Block();
			$view->block_totalvalue->totalvalue = $totalvalue;
		}		
		
		$view->conversion_id = $this->measureCodeValue->getValue('google', 'conversion_id');
		
		return $view->render();
	}
	
	public function renderTagManager()
	{
		$view = new View('tagmanager.tpl', $this->tplpath);	
		$view->code = $this->measureCodeValue->getValue('google', 'tagmanager_code');
		
		return $view->render();
	}


	/**
	 *
	 * @param array|NULL $params
	 * @return string
	 */
	public function renderConversion($params = NULL, $productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('google', 'conversion_code');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count($restrict) > 0 && count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}
		$view = new View('conversion.tpl', $this->tplpath);	
		$view->code = $code;
		
		return $view->render();
	}


	public function renderRemarketingProduct($productRestrict = FALSE)
	{
		$measures = $this->measureCodeValue->getMeasures('google', 'remarketing_code_product');
		if (count($measures) === 0) {
			return '';
		}
		if ($productRestrict) {
			$id_products = $this->measureCode->getIdProducts();
			if (is_null($id_products)) {
				return '';
			}
		}

		$code = '';
		foreach ($measures as $measure) {
			/** @var MeasureCode $measure */
			if ($productRestrict) {
				$restrict = $measure->getProductRestrict();
				$restrict = array_filter($restrict);
				if (count(array_intersect($restrict, $id_products)) === 0) {
					continue;
				}
				$code .= $measure->getValue();
			} else {
				$code .= $measure->getValue();
			}
		}

		if ($code === '') {
			return '';
		}
		$view = new View('conversion.tpl', $this->tplpath);	
		$view->code = $code;
		
		return $view->render();
	}

}
