<?php

namespace Redenge\MeasureCode;

use Redenge\MeasureCode\MeasureCode;


class MeasurecodeValue 
{
	private $core;

	/**
	 * @var string
	 */
	private $measurecode_value;

	/**
	 * @var array
	 */
	private $measureCodes;

	/**
	 * @var bool
	 */
	private $isLoaded;


	public function __construct($_core) 
	{
		$this->core = $_core;
		$this->measurecode_value = array();
		$this->measureCodes = array();
		$this->isLoaded = FALSE;
	}


	private function init() 
	{
		$select = "measurecode.code, measurecode.codetype, measurecode_type.code AS code_type, measurecode_value.*";
		
		$join = "JOIN measurecode ON measurecode.id = measurecode_value.id_measurecode";
		$join .= " JOIN measurecode_type ON measurecode_type.id = measurecode.id_measurecode_type";
		
		$filter = sprintf("measurecode_value.id_multishop=%d AND measurecode_value.id_settings_profile=%d", $this->core->ShopModel->multishop->id, $this->core->ShopModel->settings_profile->id);
		
		$records = $this->core->ShopModel->measurecode->value->getRecords($select, $join, null, $filter);
		$values = [];
		if (method_exists($this->core->ShopModel->measurecode->value, 'fetchResult')) {
			$values = $this->core->ShopModel->measurecode->value->fetchResult($records);
		} else {
			while ($row = mysqli_fetch_assoc($records))
			{
				$values[] = $row;
			}	
			mysqli_free_result($records);
		}
		foreach ($values as $row) {
			$value = $row['codetype'] == 'checkbox' ? $row['value'] == 'true' : $row['value'];
			$productRestrict = explode(';', $row['product_restrict']);
			$productRestrict = array_filter($productRestrict);
			
			$obj = (new MeasureCode)
				->setCode($row['code'])
				->setCodeType($row['code_type'])
				->setDataType($row['codetype'])
				->setValue($value)
				->setProductRestrict($productRestrict);
			$this->measureCodes[] = $obj;
		}
		
		$this->isLoaded = TRUE;
	}


	public function getValue($type, $code, $moreResult = FALSE)
	{
		if (!$this->isLoaded) {
			$this->init();
		}

		$result = [];

		/** @var MeasureCode $item */
		foreach ($this->measureCodes as $item) {
			if ($item->getCodeType() === $type && $item->getCode() === $code) {
				$result[] = $item->getValue();
			}
		}

		if (count($result) === 0) {
			return "";
		}
		if (!$moreResult) {
			return array_shift($result);
		} else {
			return implode('', $result);
		}
	}


	/**
	 * Vrátí měřící kódy podle typu a kódu
	 *
	 * @param string $type
	 * @param string $code
	 * @return array
	 */
	public function getMeasures($type, $code)
	{
		if (!$this->isLoaded) {
			$this->init();
		}

		$result = [];

		/** @var MeasureCode $item */
		foreach ($this->measureCodes as $item) {
			if ($item->getCodeType() === $type && $item->getCode() === $code) {
				$result[] = $item;
			}
		}

		return $result;
	}

}

