<!-- Custom Audience Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '{TRACKING_ID}');
fbq('track', 'PageView');

<!-- START BLOCK : event -->

fbq('track', '{PAGETYPE}', {
	
	<!-- START BLOCK : block_value -->
		value: {VALUE},
		currency: '{CURRENCY}',
	<!-- END BLOCK : block_value -->
	
	
	<!-- START BLOCK : block_pagename -->
		content_name: '{PAGENAME}',
	<!-- END BLOCK : block_pagename -->
	
	
	<!-- START BLOCK : block_pagecategory -->
		content_category: '{PAGECATEGORY}',
	<!-- END BLOCK : block_pagecategory -->
	
	
	<!-- START BLOCK : block_product -->
		content_ids: {ID_PRODUCTS},
		content_type: '{TYPE}',
	<!-- END BLOCK : block_product -->
 });
 
<!-- END BLOCK : event -->

</script>

<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id={TRACKING_ID}&ev=PageView&noscript=1"
/></noscript>
<!-- End Custom Audience Pixel Code -->