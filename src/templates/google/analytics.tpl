<!-- START - Google - analytics -->
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '{ACCOUNT_CODE}']);
	  _gaq.push(['_trackPageview']);
	  <!-- START BLOCK : ecommerce_block -->
		_gaq.push(['_addTrans',
		'{INDEX}',           // order ID - required
		'eshop',  // affiliation or store name
		'{TOTAL_PRICE}',          // total - required
		'{TAX}',           // tax
		'{DELIVERY_PRICE}',              // shipping
		'{INVOICE_CITY}',       // city
		'',     // state or province
		'{INVOICE_COUNTRY}'             // country
		]);
		<!-- START BLOCK : item -->
		_gaq.push(['_addItem',
		'{INDEX}',           // order ID - required
		'{CODE}',           // SKU/code - required
		'{NAME}',        // product name
		'',   // category or variation
		'{PRICE}',          // unit price - required
		'{ITEM_COUNT}'               // quantity - required
		]);
		<!-- END BLOCK : item -->
		_gaq.push(['_trackTrans']); //submits transaction to the Analytics servers
	  <!-- END BLOCK : ecommerce_block -->

	  (function() {

			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

	  })();
	</script>
<!-- END - Google - analytics -->	