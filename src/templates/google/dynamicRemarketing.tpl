<script type="text/javascript">
/* <![CDATA[ */
var google_tag_params = {
<!-- START BLOCK : block_ids -->
ecomm_prodid: {ID_PRODUCTS},
<!-- END BLOCK : block_ids -->
<!-- START BLOCK : block_pagetype -->
ecomm_pagetype: "{PAGETYPE}",
<!-- END BLOCK : block_pagetype -->
<!-- START BLOCK : block_totalvalue -->
ecomm_totalvalue: {TOTALVALUE},
<!-- END BLOCK : block_totalvalue -->
};
/* ]]> */
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = {CONVERSION_ID};
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/969609969/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<input type="hidden" name="google_conversion_id" value="{CONVERSION_ID}" />
