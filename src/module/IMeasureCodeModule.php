<?php

namespace Redenge\MeasureCode;

interface IMeasureCodeModule 
{
	public function getPageType();
	public function getTotalValue();
	public function getCurrency();
	public function getIdProducts();
}
