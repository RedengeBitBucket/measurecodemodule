<?php

namespace Redenge\MeasureCode;

use CoreModule;
use Strings;
use Exception;
use measurecode;

use Redenge\MeasureCode\IMeasureCodeModule;
use Redenge\MeasureCode\Presenters;

class MeasureCodeModule extends CoreModule implements IMeasureCodeModule 
{
	private $presenters;
	private $core;
	private $model;
	private $idProducts;
	private $pageType;
	private $pageName;
	private $pageCategory;
	private $totalvalue;
	private $currency;
	
	public function init(\Core $_core) 
	{
		$this->presenters = array();
		
		$this->core = $_core;
		$this->core->ShopModel->addModel('measurecode', new measurecode($this->core->DB, $this->core->ShopModel));
		
		$this->measureCodeValue = new MeasurecodeValue($this->core);
	}
	
	function __get($_var) 
	{
		if (property_exists($this, $_var)) 
		{
			return $this->$_var;
		}
		
		$class = 'Redenge\\MeasureCode\\Presenters\\' . ucfirst($_var) . 'Presenter';
		
		if (class_exists($class)) 
		{
			if (!isset($this->presenters[$_var])) 
			{
				$this->presenters[$_var] = new $class($this);
			}

			return $this->presenters[$_var];
		} 
		else
			throw new Exception("Presenter $_var is not defined.");
	}

	public function addPageType($_pagetype)
	{
		if(Strings::length($_pagetype) > 0)
		{
			$this->pageType = $_pagetype;
		}
	}
	
	public function addPageName($_pagename)
	{
		if(Strings::length($_pagename) > 0)
		{
			$this->pageName = $_pagename;
		}
	}
	
	public function addPageCategory($_pagecategory)
	{
		if(count($_pagecategory) > 0)
		{
			$this->pageCategory = $_pagecategory;
		}
	}
	
	public function addProduct($_idProduct)
	{
		if(is_array($_idProduct))
		{
			foreach($_idProduct as $id)
			{
				$this->idProducts[] = $id;
			}
		}
		else
		{
			$this->idProducts[] = $_idProduct;
		}
	}
	
	public function addTotalValue($_totalvalue)
	{
		if(!is_null($_totalvalue))
		{
			$this->totalvalue = $_totalvalue;	
		}
	}
	
	public function addCurrency($_code)
	{
		if(!is_null($_code))
		{
			$this->currency = $_code;	
		}
	}

	public function getPageType() 
	{
		return $this->pageType;
	}
	
	public function getPageName() 
	{
		return $this->pageName;
	}
	
	public function getPageCategory() 
	{
		return $this->pageCategory;
	}
	
	public function getIdProducts()
	{
		return $this->idProducts;		
	}
	
	public function getTotalValue()
	{
		return $this->totalvalue;
	}
	
	public function getCurrency()
	{
		return $this->currency;
	}

}
