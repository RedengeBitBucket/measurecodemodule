<?php

namespace Redenge\MeasureCode;


class MeasureCode
{

	/**
	 * Kód měřícího kódu
	 * @var string
	 */
	private $code;

	/**
	 * Google, Seznam, Facebook, ...
	 * @var string
	 */
	private $codeType;

	/**
	 * Datový typ [text, textarea, ..]
	 * @var string
	 */
	private $dataType;

	/**
	 * Omezení na množinu produktů
	 * @var array
	 */
	private $productRestrict;

	/**
	 * Skript (hodnota) měřícího kódu
	 * @var string
	 */
	private $value;


	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}


	/**
	 * @return string
	 */
	public function getCodeType()
	{
		return $this->codeType;
	}


	/**
	 * @return string
	 */
	public function getDataType()
	{
		return $this->dataType;
	}


	/**
	 * @return array
	 */
	public function getProductRestrict()
	{
		return $this->productRestrict;
	}


	/**
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}


	/**
	 * @param string $code
	 * @return \Redenge\MeasureCode\MeasureCode
	 */
	public function setCode($code)
	{
		$this->code = $code;
		return $this;
	}


	/**
	 * @param string $codeType
	 * @return \Redenge\MeasureCode\MeasureCode
	 */
	public function setCodeType($codeType)
	{
		$this->codeType = $codeType;
		return $this;
	}


	/**
	 * @param string $dataType
	 * @return \Redenge\MeasureCode\MeasureCode
	 */
	public function setDataType($dataType)
	{
		$this->dataType = $dataType;
		return $this;
	}


	/**
	 * @param array $productRestrict
	 * @return \Redenge\MeasureCode\MeasureCode
	 */
	public function setProductRestrict(Array $productRestrict)
	{
		$this->productRestrict = $productRestrict;
		return $this;
	}


	/**
	 * @param string $value
	 * @return \Redenge\MeasureCode\MeasureCode
	 */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}

}
