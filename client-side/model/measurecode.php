<?php
class measurecode extends DBObject
{

	function __construct($database, $parentNode = NULL)
	{
		parent::__construct($database, 'measurecode', $parentNode);

		parent::createString('code');
		parent::createString('codetype');
		parent::createString('name');
		parent::createInteger('id_measurecode_type');

		parent::createObject('type', 'measurecode_type');
		parent::createObject('value', 'measurecode_value');
	}

}

class measurecode_type extends DBObject
{

	function __construct($database, $parentNode = NULL)
	{
		parent::__construct($database, 'measurecode_type', $parentNode);

		parent::createString('code');
		parent::createString('name');
	}

}

class measurecode_value extends DBObject
{

	function __construct($database, $parentNode = NULL)
	{
		parent::__construct($database, 'measurecode_value', $parentNode);

		parent::createString('value');
		parent::createString('product_restrict');
		parent::createInteger('id_measurecode');
		parent::createInteger('id_multishop');
		parent::createInteger('id_settings_profile');
	}

}
